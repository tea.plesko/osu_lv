#Napišite program koji od korisnika zahtijeva 
#upis jednog broja koji predstavlja nekakvu ocjenu i 
#nalazi se izmed ̄u 0.0 i 1.0. Ispišite kojoj 
#kategoriji pripada ocjena na temelju sljedec ́ih uvjeta:

try:
    n = float(input("Unesi broj između 0.0 i 1.0: "))
except ValueError:
    print("Can't be empty number")

if n < 0.0 or n > 1.0:
    nError = ValueError("n must be between 0.0 and 1.0")
    raise nError

if (n >= 0.9):
    print("A")
elif (n >= 0.8):
    print("B")
elif (n >= 0.7):
    print("C")
elif (n >= 0.6):
    print("D")
else:
    print("F")