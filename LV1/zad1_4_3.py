# Napišite program koji od korisnika zahtijeva unos brojeva u
# beskonacˇnoj petlji sve dok korisnik ne upiše „Done“ 
# (bez navodnika). Pri tome brojeve spremajte u listu. 
# Nakon toga potrebno je ispisati koliko brojeva je korisnik 
# unio, njihovu srednju, minimalnu i maksimalnu vrijednost. 
# Sortirajte listu i ispišite je na ekran. Dodatno: osigurajte 
# program od pogrešnog unosa (npr. slovo umjesto brojke) na 
# nacˇin da program zanemari taj unos i ispiše odgovarajuc ́u 
# poruku.

numbers= []

while True:
    x = input("Enter a number:")
    if (x == 'done'): 
        break
    try:
        x = int(x)
    except:
        print("You've entered the letter")
        continue
    
    numbers.append(x)

print ("Korisnik je unio: %d brojeva." %len(numbers))

print ("Minimalna vrijednost: %d" %min(numbers))
print ("Maksimalna vrijednost: %d" %max(numbers))

print ("Srednja vrijednost: %.2f" %(sum(numbers)/len(numbers)))

print(numbers)

numbers.sort()

print(numbers)