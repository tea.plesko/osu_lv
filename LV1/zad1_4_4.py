# Napišite Python skriptu koja ce ucitati tekstualnu datoteku naziva song.txt.
# Potrebno je napraviti rjecnik koji kao kljuceve koristi sve razlicite rijeci koje se pojavljuju u
# datoteci, dok su vrijednosti jednake broju puta koliko se svaka rijec (kljuc) pojavljuje u datoteci.
# Koliko je rijeci koje se pojavljuju samo jednom u datoteci? Ispišite ih.

file = open ('song.txt')

word_dict = {}

for line in file:
    line = line.rstrip ()
    words = line.split ()
    for word in words:
        word = word.replace(",","")
        if word in word_dict:
            word_dict[word] += 1
        else:
            word_dict[word] = 1

counter = 0

for word in word_dict:
    if word_dict[word] == 1:
        print(word)
        counter += 1

print(f"{counter} riječi se pojavljuju samo jednom")

file.close ()