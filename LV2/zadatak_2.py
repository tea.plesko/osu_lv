"""Zadatak 2.4.2 Datoteka data.csv sadrži mjerenja visine i mase provedena na muškarcima i
 ženama. Skripta zadatak_2.py ucitava dane podatke u obliku numpy polja data pricemu je u
 prvom stupcu polja oznaka spola (1 muško, 0 žensko), drugi stupac polja je visina u cm, a treci
 stupac polja je masa u kg.
 a) Na temelju velicine numpy polja data, na koliko osoba su izvršena mjerenja?
 b) Prikažite odnos visine i mase osobe pomocu naredbe matplotlib.pyplot.scatter.
 c) Ponovite prethodni zadatak, ali prikažite mjerenja za svaku pedesetu osobu na slici.
 d) Izracunajte i ispišite u terminal minimalnu, maksimalnu i srednju vrijednost visine u ovom
 podatkovnom skupu.
 e) Ponovite zadatak pod d), ali samo za muškarce, odnosno žene. Npr. kako biste izdvojili
 muškarce, stvorite polje koje zadrži bool vrijednosti i njega koristite kao indeks retka.
 ind = (data[:,0] == 1)"""


import numpy as np
from numpy import genfromtxt
import matplotlib.pyplot as plt


my_data = genfromtxt('data.csv', delimiter=',')
my_data = my_data[1:]

# a)
print('Mjerenja su izvršena na %d osoba.' %len(my_data))
# b)
a = my_data[:,1] 
# : indicates that we're selecting all rows
# ,1 indicates that we're selecting the second column (indexing starts from 0)
b = my_data[:,2]
#So, b = my_data[:,2] selects all rows from the third column of my_data and 
# assigns it to the variable b

# c)
a50 = my_data[49::50,1]
b50 = my_data[49::50,2]

plt.scatter(a,b)
plt.xlabel('Visina')
plt.ylabel('Masa')
plt.title('Odnos visine i mase')
plt.show()

plt.scatter(a50,b50)
plt.xlabel('Visina')
plt.ylabel('Masa')
plt.title('Odnos visine i mase za svaku 50.-u osobu')
plt.show()

# d)
print("Maksimalna visina: %.2f" %a.max())
print('Minimalna visina: %.2f' %a.min())
print('Srednja vrijednost visine: %.2f' %a.mean())

# e)
muskarci = my_data[(my_data[:,0] == 1)]
m_visina = muskarci[:,1]
print("Maksimalna visina muškaraca: %.2f" %m_visina.max())
print('Minimalna visina muškaraca: %.2f' %m_visina.min())
print('Srednja vrijednost visine muškaraca: %.2f' %m_visina.mean())

zene = my_data[(my_data[:,0] == 0)]
z_visina = zene[:,1]
print("Maksimalna visina žena: %.2f" %z_visina.max())
print('Minimalna visina žena: %.2f' %z_visina.min())
print('Srednja vrijednost visine žena: %.2f' %z_visina.mean())
  

    
