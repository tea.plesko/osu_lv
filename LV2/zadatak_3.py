"""Zadatak 2.4.3 Skripta zadatak_3.py ucitava sliku 'road.jpg'. Manipulacijom odgovarajuce
 numpy matrice pokušajte:
 a) posvijetliti sliku,
 b) prikazati samo drugu cetvrtinu slike po širini,
 c) zarotirati sliku za 90 stupnjeva u smjeru kazaljke na satu,
 d) zrcaliti sliku."""


import numpy as np
import matplotlib.pyplot as plt

img = plt.imread("road.jpg")
img = img[:,:,0].copy()
# first : indicates that you're selecting all rows
# second : indicates that you're selecting all rows
# 0 indicates that you're selecting the first channel or layer of the image (channels are typically used in color images to represent red, green, and blue channels)
# .copy() creates a deep copy of the selected part of the array

plt.figure()
plt.imshow(img, cmap='gray')
plt.show()

img_b=img.copy()
img_b+=70 # Increases the brightness of the copied image by adding 70 to all pixel values
img_b[img_b<70] = 255 # Sets all pixel values below 70 to 255, effectively adjusting the brightness
plt.imshow(img_b, cmap="gray") 
plt.show() # displays the plot

img2=img[:,160:320]
plt.imshow(img2, cmap="gray")
plt.show()

img_rot=np.rot90(img)
img_rot=np.rot90(img_rot)
img_rot=np.rot90(img_rot)

plt.imshow(img_rot, cmap="gray")
plt.show()

img_mirror=np.fliplr(img) # Mirrors the image horizontally
plt.imshow(img_mirror, cmap="gray")
plt.show()

"""
import numpy as np
import matplotlib.pyplot as plt

image = plt.imread("road.jpg")
image = image[:,:,0].copy()

height, width = image.shape

second_quarter_image = image[:, width // 4 : width // 2]
rotated_image = np.rot90(image, k=3)
flipped_image = np.flipud(image) flips vertically (upside down)

plt.figure(figsize=(10, 6))

plt.subplot(231), plt.imshow(image, cmap="gray"), plt.title('Originalna slika')
plt.subplot(232), plt.imshow(image, alpha=0.6, cmap="gray"), plt.title('Posvijetljena slika')
plt.subplot(233), plt.imshow(second_quarter_image, cmap="gray"), plt.title('Druga četvrtina slike')
plt.subplot(234), plt.imshow(rotated_image, cmap="gray"), plt.title('Rotirana slika')
plt.subplot(235), plt.imshow(flipped_image, cmap="gray"), plt.title('Zrcaljena slika')

plt.show()
"""