"""Zadatak 2.4.4 Napišite program kojice kreirati sliku koja sadrži cetiri kvadrata crne odnosno
 bijele boje (vidi primjer slike 2.4 ispod). Za kreiranje ove funkcije koristite numpy funkcije
 zeros i ones kako biste kreirali crna i bijela polja dimenzija 50x50 piksela. Kako biste ih složili
 u odgovarajuci oblik koristite numpy funkcije hstack i vstack."""


import numpy as np
import matplotlib.pyplot as plt

black=np.zeros((50,50))
white=np.full((50,50), 255) # creates a new NumPy array with dimensions 50x50, filled with the value 255

upper = np.hstack((black, white))
lower = np.hstack((white, black))
img = np.vstack((upper, lower))

plt.imshow(img, cmap="gray")
plt.show()
