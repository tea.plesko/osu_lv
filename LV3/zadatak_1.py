""" Zadatak 3.4.1 Skripta zadatak_1.py ucitava podatkovni skup iz data_C02_emission.csv.
 Dodajte programski kod u skriptu pomocu kojeg možete odgovoriti na sljedeca pitanja:
 a) Koliko mjerenja sadrži DataFrame? Kojeg je tipa svaka velicina? Postoje li izostale ili
 duplicirane vrijednosti? Obrišite ih ako postoje. Kategoricke velicine konvertirajte u tip
 category.
 b) Koja tri automobila ima najvecu odnosno najmanju gradsku potrošnju? Ispišite u terminal:
 ime proizvodaca, model vozila i kolika je gradska potrošnja.
 c) Koliko vozila ima velicinu motora izmedu 2.5 i 3.5 L? Kolika je prosjecna C02 emisija
 plinova za ova vozila?
 d) Koliko mjerenja se odnosi na vozila proizvodaca Audi? Kolika je prosjecna emisija C02
 plinova automobila proizvodaca Audi koji imaju 4 cilindara?
 e) Koliko je vozila s 4,6,8... cilindara? Kolika je prosjecna emisija C02 plinova s obzirom na
 broj cilindara?
 f) Kolika je prosjecna gradska potrošnja u slucaju vozila koja koriste dizel, a kolika za vozila
 koja koriste regularni benzin? Koliko iznose medijalne vrijednosti?
 g) Koje vozilo s 4 cilindra koje koristi dizelski motor ima najvecu gradsku potrošnju goriva?
 h) Koliko ima vozila ima rucni tip mjenjaca (bez obzira na broj brzina)?
 i) Izracunajte korelaciju izmedu numerickih velicina. Komentirajte dobiveni rezultat.
 """


import pandas as pd
import numpy as np

data = pd.read_csv('data_C02_emission.csv')

# a)
print(f'Broj mjerenja: {len(data)}')

print(data.dtypes)

data = data.dropna()
data = data.drop_duplicates()

print(f'Postoje duplicirane vrijednosti: {data}')

columns_for_convert = ['Make', 'Model', 'Vehicle Class', 'Transmission', 'Fuel Type', ]
for column in columns_for_convert:
    data[column] = data[column].astype('category')

# b)
fuel_consumption_city = data[['Make', 'Model', 'Fuel Consumption City (L/100km)']].sort_values(by='Fuel Consumption City (L/100km)')
print(f'Najmanja gradska potrosnja: {fuel_consumption_city.head(3)}')
print(f'Najveca gradska potrosnja: {fuel_consumption_city.tail(3)}')

# c)
engine_size = data[(data['Engine Size (L)'] > 2.5) & (data['Engine Size (L)'] < 3.5)]
print(f"{len(engine_size)} vozila ima veličinu motora između 2.5 i 3.5")
print(f"Prosječna CO2 emisija za ova vozila je {engine_size['CO2 Emissions (g/km)'].mean()}")

# d)
audi_vehicles = data[data['Make'] == 'Audi']
print(f"{len(audi_vehicles)} mjerenja se odnosi na proizvođača Audi")

audi_4_cylinders = audi_vehicles[audi_vehicles['Cylinders'] == 4]
print(f"Prosječna emisija CO2 plinova automobila proizvođača Audi koji imaju 4 cilindra izosi: {audi_4_cylinders['CO2 Emissions (g/km)'].mean()}")

# e)
print()

grouped_cylinders = data.groupby('Cylinders')
for number_of_cylinders, group in grouped_cylinders:
    print(f"Prosječna emisija CO2 plinova za vozila sa {number_of_cylinders} cilindara: {group['CO2 Emissions (g/km)'].mean()}")

# f)
print()

dizel = data[data['Fuel Type'] == 'D']
regular_gasoline = data[data['Fuel Type'] == 'X']

print(f"Prosječna gradska potrošnja u slučaju vozila koja koriste dizel iznosi: {dizel['Fuel Consumption City (L/100km)'].mean()}")
print(f"Prosječna gradska potrošnja u slučaju vozila koja koriste regularni benzin iznosi: {regular_gasoline['Fuel Consumption City (L/100km)'].mean()}")

print(f"Medijalna vrijednost gradske potrošnja u slučaju vozila koja koriste dizel iznosi: {dizel['Fuel Consumption City (L/100km)'].median()}")
print(f"Medijalna vrijednost gradske potrošnja u slučaju vozila koja koriste regularni benzin iznosi: {regular_gasoline['Fuel Consumption City (L/100km)'].median()}")

# g)
print()

cylinders_4 = data[data['Cylinders'] == 4]
dizel_4_cylinders = cylinders_4[cylinders_4['Fuel Type'] == 'D']

dizel_4_cylinders.sort_values(by='Fuel Consumption City (L/100km)')
print(dizel_4_cylinders.head(1))

# h)
manual = data[data['Transmission'].str[0] == 'M']
print (f"Ručni tip mjenjača ima {len(manual)} automobila")

# i)
data_corr = data.corr(numeric_only=True)
print(data_corr)