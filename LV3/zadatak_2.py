"""Zadatak 3.4.2 Napišite programski kod kojice prikazati sljedece vizualizacije:
 a) Pomocu histograma prikažite emisiju C02 plinova. Komentirajte dobiveni prikaz.
 b) Pomocu dijagrama raspršenja prikažite odnos izmedu gradske potrošnje goriva i emisije
 C02 plinova. Komentirajte dobiveni prikaz. Kako biste bolje razumjeli odnose izmedu
 velicina, obojite tockice na dijagramu raspršenja s obzirom na tip goriva.
 c) Pomocu kutijastog dijagrama prikažite razdiobu izvangradske potrošnje s obzirom na tip
 goriva. Primjecujete li grubu mjernu pogrešku u podacima?
 d) Pomocu stupcastog dijagrama prikažite broj vozila po tipu goriva. Koristite metodu
 groupby.
 e) Pomocu stupcastog grafa prikažite na istoj slici prosjecnu C02 emisiju vozila s obzirom na
 broj cilindara.
 """


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

data = pd.read_csv('data_C02_emission.csv')

columns_for_convert = ['Make', 'Model', 'Vehicle Class', 'Transmission', 'Fuel Type', ]
for column in columns_for_convert:
    data[column] = data[column].astype('category')

# a)
plt.figure()
plt.hist(data['CO2 Emissions (g/km)'], bins=50)
plt.show()

# b)
plt.figure()
plt.scatter(data['Fuel Consumption City (L/100km)'], data['CO2 Emissions (g/km)'], c=data['Fuel Type'].cat.codes)
plt.show()

'''
#b)
fuels = {'X' : 'blue', 'Z' : 'pink', 'D' : 'red', 'E' : 'yellow', 'N' : 'green'}
plt.figure(figsize=(10,6))
for fuel_type, color in fuels.items():
    subset = data[data['Fuel Type'] == fuel_type]
    plt.scatter(subset['Fuel Consumption City (L/100km)'], subset['CO2 Emissions (g/km)'],
                color=color, label=fuel_type)
plt.legend(title='Fuel Type')
plt.xlabel('Fuel Consumption City (L/100km)')
plt.ylabel('CO2 Emissions (g/km)')
plt.show()

objasnjenje:
fuels: This dictionary maps fuel types to colors.
plt.figure(figsize=(10,6)): This line creates a new figure for plotting with Matplotlib, specifying the size of the figure as 10 inches in width and 6 inches in height.
for fuel_type, color in fuels.items():: This loop iterates over each key-value pair in the fuels dictionary.
subset = data[data['Fuel Type'] == fuel_type]: This line selects a subset of the data where the 'Fuel Type' column matches the current fuel type.
plt.scatter(subset['Fuel Consumption City (L/100km)'], subset['CO2 Emissions (g/km)'], color=color, label=fuel_type): This line creates a scatter plot using the 'Fuel Consumption City (L/100km)' column on the x-axis and the 'CO2 Emissions (g/km)' column on the y-axis. Each point is colored according to the fuel type and labeled accordingly.
plt.legend(title='Fuel Type'): This line adds a legend to the plot with the title 'Fuel Type', which corresponds to the labels specified in the scatter plot.
plt.xlabel('Fuel Consumption City (L/100km)'): This line sets the label for the x-axis.
plt.ylabel('CO2 Emissions (g/km)'): This line sets the label for the y-axis.
plt.show(): This line displays the plot
'''

# c)
data.boxplot(column='Fuel Consumption Hwy (L/100km)', by='Fuel Type')
plt.show()
 
# d)
data_grouped = data.groupby('Fuel Type').size()
data_grouped.plot(kind='bar')
plt.show()

# e)
avg_co2_emission_per_cylinder = data.groupby('Cylinders')['CO2 Emissions (g/km)'].mean()
avg_co2_emission_per_cylinder.plot(kind='bar')
plt.show()
