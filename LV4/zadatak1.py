""" Zadatak 4.5.1 Skripta zadatak_1.py ucitava podatkovni skup iz data_C02_emission.csv.
Potrebno je izgraditi i vrednovati model koji procjenjuje emisiju C02 plinova na temelju os
talih numerickih ulaznih velicina. Detalje oko ovog podatkovnog skupa mogu se pronaci u 3.
laboratorijskoj vježbi.
 a) Odaberite željene numericke velicine specificiranjem liste s nazivima stupaca. Podijelite
 podatke na skup za ucenje i skup za testiranje u omjeru 80%-20%.
 b) Pomocu matplotlib biblioteke i dijagrama raspršenja prikažite ovisnost emisije C02 plinova
 o jednoj numerickoj velicini. Pri tome podatke koji pripadaju skupu za ucenje oznacite
 plavom bojom, a podatke koji pripadaju skupu za testiranje oznacite crvenom bojom.
 c) Izvršite standardizaciju ulaznih velicina skupa za ucenje. Prikažite histogram vrijednosti
 jedne ulazne velicine prije i nakon skaliranja. Na temelju dobivenih parametara skaliranja
 transformirajte ulazne velicine skupa podataka za testiranje.
 d) Izgradite linearni regresijski modeli. Ispišite u terminal dobivene parametre modela i
 povežite ih s izrazom 4.6.
 e) Izvršite procjenu izlazne velicine na temelju ulaznih velicina skupa za testiranje. Prikažite
 pomocu dijagrama raspršenja odnos izmedu stvarnih vrijednosti izlazne velicine i procjene
 dobivene modelom.
 f) Izvršite vrednovanje modela na nacin da izracunate vrijednosti regresijskih metrika na
 skupu podataka za testiranje.
 g) Što se dogada s vrijednostima evaluacijskih metrika na testnom skupu kada mijenjate broj
 ulaznih velicina?"""


import pandas as pd
import matplotlib.pyplot as plt
from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import StandardScaler
import sklearn.linear_model as lm
from sklearn.metrics import mean_absolute_error, mean_squared_error, r2_score

data = pd.read_csv('data_C02_emission.csv')

X = data[['Engine Size (L)', 'Cylinders','Fuel Consumption City (L/100km)','Fuel Consumption Hwy (L/100km)','Fuel Consumption Comb (L/100km)','Fuel Consumption Comb (mpg)']]

y = data['CO2 Emissions (g/km)']

#a
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

#b
plt.figure()

plt.scatter(X_train['Engine Size (L)'], y_train, color='blue')
plt.scatter(X_test['Engine Size (L)'], y_test, color='red')

plt.show()

#c
scaler = StandardScaler()

X_train_scaled = scaler.fit_transform(X_train)

X_test_scaled = scaler.transform(X_test)

X_train_scaled = pd.DataFrame(X_train_scaled, columns=X_train.columns)
X_test_scaled = pd.DataFrame(X_test_scaled, columns=X_test.columns)

plt.figure()
plt.hist(X_train['Engine Size (L)'], bins=20, color='blue', alpha=0.7, label='X_train')
plt.hist(X_train_scaled['Engine Size (L)'], bins=20, color='red', alpha=0.7, label='X_train_scaled')
plt.show()

'''
plt.figure(figsize=(10, 6))

plt.subplot(1, 2, 1)
plt.hist(X_train['Cylinders'], bins=20)
plt.title('Histogram cilindara prije skaliranja')
plt.xlabel('Broj cilindara')
plt.ylabel('Broj vozila')

standard_scaler = StandardScaler()
minmax_scaler = MinMaxScaler()
scaled_X_train = minmax_scaler.fit_transform(X_train)
scaled_X_train_df = pd.DataFrame(scaled_X_train, columns=X_train.columns)

plt.subplot(1, 2, 2)
plt.hist(scaled_X_train_df['Cylinders'], bins=20, color='red')
plt.title('Histogram cilindara nakon skaliranja')
plt.xlabel('Skalirani broj cilindara')
plt.ylabel('Broj vozila')

plt.tight_layout()
plt.show()

scaled_X_test = minmax_scaler.transform(X_test)
scaled_X_test_df = pd.DataFrame(scaled_X_test, columns=X_test.columns)

objasnjenje:
plt.figure(figsize=(10, 6)): This line creates a new figure for plotting with Matplotlib, specifying the size of the figure as 10 inches in width and 6 inches in height.
plt.subplot(1, 2, 1): This line creates the first subplot in a grid with 1 row and 2 columns, and selects the first subplot for plotting.
plt.hist(X_train['Cylinders'], bins=20): This line plots a histogram of the 'Cylinders' column from the training data (X_train). It divides the data into 20 bins.
plt.title('Histogram cilindara prije skaliranja'): This line sets the title for the first subplot.
plt.xlabel('Broj cilindara'): This line sets the label for the x-axis.
plt.ylabel('Broj vozila'): This line sets the label for the y-axis.
standard_scaler = StandardScaler() and minmax_scaler = MinMaxScaler(): These lines create instances of the StandardScaler and MinMaxScaler from Scikit-learn.
scaled_X_train = minmax_scaler.fit_transform(X_train): This line scales the training data using the MinMaxScaler.
scaled_X_train_df = pd.DataFrame(scaled_X_train, columns=X_train.columns): This line converts the scaled training data into a DataFrame with the same column names as X_train.
plt.subplot(1, 2, 2): This line creates the second subplot in the same grid and selects it for plotting.
plt.hist(scaled_X_train_df['Cylinders'], bins=20, color='red'): This line plots a histogram of the 'Cylinders' column from the scaled training data (scaled_X_train_df). It divides the data into 20 bins and sets the color of the bars to red.
plt.title('Histogram cilindara nakon skaliranja'): This line sets the title for the second subplot.
plt.xlabel('Skalirani broj cilindara'): This line sets the label for the x-axis.
plt.ylabel('Broj vozila'): This line sets the label for the y-axis.
plt.tight_layout(): This line adjusts the spacing between subplots to prevent overlapping text or labels.
plt.show(): This line displays the figure with the two subplots
'''

#d
model = LinearRegression()
model.fit(X_train_scaled, y_train)

print("Intercept:", model.intercept_)
print("Coefficients:", model.coef_)

'''
print("Intercept:", model.intercept_): This line prints the intercept term of the fitted linear regression model. The intercept is the value of the predicted target variable when all input features are zero.
print("Coefficients:", model.coef_): This line prints the coefficients of the input features in the fitted linear regression model. Each coefficient represents the change in the predicted target variable for a one-unit change in the corresponding input feature, holding all other features constant
'''

#e
y_pred = model.predict(X_test_scaled)

plt.figure()
plt.scatter(y_test, y_pred, color='blue')
plt.xlabel('Actual CO2 Emissions (g/km)')
plt.ylabel('Predicted CO2 Emissions (g/km)')
plt.show()

#f
mae = mean_absolute_error(y_test, y_pred)
mse = mean_squared_error(y_test, y_pred)
r2 = r2_score(y_test, y_pred)

print("Mean Absolute Error (MAE):", mae)
print("Mean Squared Error (MSE):", mse)
print("R-squared (R2) Score:", r2)

'''
MAE = mean_absolute_error(y_test, y_test_prediction)
MAPE = mean_absolute_percentage_error(y_test, y_test_prediction)
MSE = mean_squared_error(y_test, y_test_prediction)
RMSE = math.sqrt(MSE)
R2 = r2_score(y_test, y_test_prediction)
print(f"MAE: {MAE}")
print(f"MAPE: {MAPE}")
print(f"MSE: {MSE}")
print(f"RMSE: {RMSE}")
print(f"R2: {R2}")

objasnjenje:
MAE = mean_absolute_error(y_test, y_test_prediction): This line computes the Mean Absolute Error (MAE) 
between the actual target values (y_test) and the predicted target values (y_test_prediction). 
MAE measures the average absolute difference between the predicted and actual values.

MAPE = mean_absolute_percentage_error(y_test, y_test_prediction): This line computes the Mean Absolute 
Percentage Error (MAPE) between the actual target values and the predicted target values. MAPE measures 
the average percentage difference between the predicted and actual values.


MSE = mean_squared_error(y_test, y_test_prediction): This line computes the Mean Squared Error (MSE) 
between the actual target values and the predicted target values. MSE measures the average squared 
difference between the predicted and actual values.

RMSE = math.sqrt(MSE): This line computes the Root Mean Squared Error (RMSE) by taking the square root 
of the MSE. RMSE is a commonly used metric that measures the square root of the average squared difference
between the predicted and actual values.

R2 = r2_score(y_test, y_test_prediction): This line computes the coefficient of determination (R-squared) 
between the actual target values and the predicted target values. R-squared measures the proportion of the 
variance in the target variable that is explained by the model
'''

#g) Značajnija razlika dobije se tek kada ostane samo jedna ulazna veličina. 
# Već s dvije i više ulaznih veličina model poprilično dobro opisuje podatke.
