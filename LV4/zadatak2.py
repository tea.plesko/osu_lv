""" Zadatak 4.5.2 Na temelju rješenja prethodnog zadatka izradite model koji koristi i kategoricku
 varijable „Fuel Type“ kao ulaznu velicinu. Pri tome koristite 1-od-K kodiranje kategorickih
 velicina. Radi jednostavnosti nemojte skalirati ulazne velicine. Komentirajte dobivene rezultate.
 Kolika je maksimalna pogreška u procjeni emisije C02 plinova u g/km? O kojem se modelu
 vozila radi?"""


import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler, OneHotEncoder
from sklearn.compose import ColumnTransformer
from sklearn.pipeline import Pipeline
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_absolute_error, mean_squared_error, r2_score

data = pd.read_csv('data_C02_emission.csv')

X = data[['Engine Size (L)', 'Cylinders', 'Fuel Type','Fuel Consumption City (L/100km)','Fuel Consumption Hwy (L/100km)','Fuel Consumption Comb (L/100km)','Fuel Consumption Comb (mpg)']]

y = data['CO2 Emissions (g/km)']

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

numeric_features = ['Engine Size (L)', 'Cylinders','Fuel Consumption City (L/100km)','Fuel Consumption Hwy (L/100km)','Fuel Consumption Comb (L/100km)','Fuel Consumption Comb (mpg)']

categoric_features = ['Fuel Type']

numeric_transformer = Pipeline(steps=[
    ('scaler', StandardScaler())])

categoric_transformer = Pipeline(steps=[
    ('onehot', OneHotEncoder(handle_unknown='ignore'))])

preprocessor = ColumnTransformer(
    transformers=[
        ('num', numeric_transformer, numeric_features),
        ('cat', categoric_transformer, categoric_features)])

model = Pipeline(steps=[('preprocessor', preprocessor),
                        ('regressor', LinearRegression())])

model.fit(X_train, y_train)

y_pred = model.predict(X_test)
mae = mean_absolute_error(y_test, y_pred)
mse = mean_squared_error(y_test, y_pred)
r2 = r2_score(y_test, y_pred)


print("Mean Absolute Error (MAE):", mae)
print("Mean Squared Error (MSE):", mse)
print("R-squared (R2) Score:", r2)

'''
data = pd.read_csv('data_C02_emission.csv')
ohe = OneHotEncoder()
X_encoded = ohe.fit_transform(data[['Fuel Type']]).toarray()

numerical_features = data.select_dtypes(include='number')
ohe_columns = ohe.get_feature_names_out(['Fuel Type'])
X_encoded_df = pd.DataFrame(X_encoded, columns=ohe_columns, index=data.index)
numerical_features = pd.concat([numerical_features, X_encoded_df], axis=1)

X_train, X_test, y_train, y_test = train_test_split(numerical_features.drop(['CO2 Emissions (g/km)'], axis=1), 
                                                    numerical_features['CO2 Emissions (g/km)'], test_size=0.2, random_state=1)

linearModel = lm.LinearRegression()
linearModel.fit(X_train, y_train)
print("Model coefficients:")
print(linearModel.coef_)


y_test_prediction = linearModel.predict(X_test)
plt.scatter(y_test, y_test_prediction)
plt.title('Real Values vs. Predicted values')
plt.xlabel("Real Values")
plt.ylabel("Predicted Values")
plt.grid(True)
plt.show()

absolute_errors = abs(y_test - y_test_prediction)

max_error_index = absolute_errors.idxmax()
max_error = absolute_errors[max_error_index]

vehicle_model = data.loc[max_error_index, 'Model']

print(f"Maximum absolute error: {max_error}")
print(f"Model of the vehicle associated with maximum error: {vehicle_model}")

'''