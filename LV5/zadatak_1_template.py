"""Zadatak 5.5.1 Skripta zadatak_1.py generira umjetni binarni klasifikacijski problem s dvije
 ulazne veliˇ cine. Podaci su podijeljeni na skup za uˇ cenje i skup za testiranje modela.
 a) Prikažite podatke za uˇ cenje u x1-x2 ravnini matplotlib biblioteke pri ˇ cemu podatke obojite
 s obzirom na klasu. Prikažite i podatke iz skupa za testiranje, ali za njih koristite drugi
 marker (npr. 'x'). Koristite funkciju scatter koja osim podataka prima i parametre c i
 cmap kojima je moguce definirati boju svake klase.
 b) Izgradite model logistiˇ cke regresije pomocu scikit-learn biblioteke na temelju skupa poda
taka za uˇ cenje.
 c) Prona¯ dite u atributima izgra¯ denog modela parametre modela. Prikažite granicu odluke
 nauˇcenog modela u ravnini x1-x2 zajedno s podacima za uˇcenje. Napomena: granica
 odluke u ravnini x1-x2 definirana je kao krivulja: θ0+θ1x1+θ2x2 = 0.
 d) Provedite klasifikaciju skupa podataka za testiranje pomocu izgra¯ denog modela logistiˇ cke
 regresije. Izraˇ cunajte i prikažite matricu zabune na testnim podacima. Izraˇ cunate toˇ cnost,
 preciznost i odziv na skupu podataka za testiranje.
 e) Prikažite skup za testiranje u ravnini x1-x2. Zelenom bojom oznaˇ cite dobro klasificirane
 primjere dok pogrešno klasificirane primjere oznaˇ cite crnom bojom."""


import numpy as np
import matplotlib
import matplotlib.pyplot as plt
 
from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score, classification_report
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay
 
 
X, y = make_classification(n_samples=200, n_features=2, n_redundant=0, n_informative=2,
                            random_state=213, n_clusters_per_class=1, class_sep=1)

'''
n_samples=200: This specifies the number of samples in the dataset. In this case, there are 200 samples.
n_features=2: This specifies the number of features (or dimensions) in the dataset. Each sample will have two features.
n_redundant=0: This specifies the number of redundant features. Redundant features are linear combinations of the informative features and thus do not add information to the dataset.
n_informative=2: This specifies the number of informative features. These are the features that actually influence the classification of each sample.
random_state=213: This sets the random seed for reproducibility. Setting the random state ensures that the same dataset is generated each time the code is run.
n_clusters_per_class=1: This specifies the number of clusters per class. In this case, each class has only one cluster.
class_sep=1: This parameter controls the separation between classes. Larger values indicate greater separation between classes.
The function returns X, which contains the feature vectors of the generated samples, and y, which contains the corresponding class labels for each sample.

In summary, this code generates a synthetic classification dataset with two informative features, no redundant features, 200 samples, and good separation between classes.
'''
 
# train test split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=5)
 
 
# a)
plt.figure()
plt.scatter(X_train[:, 0], X_train[:, 1], marker='o', cmap = 'viridis', c=y_train)
plt.scatter(X_test[:, 0], X_test[:, 1], marker='x', cmap='plasma', c=y_test)
 
# b)
LogRegression = LogisticRegression()
LogRegression.fit(X_train, y_train)
 
# c)
parameters = LogRegression.coef_
intercept = LogRegression.intercept_
 
theta1 = parameters[0][0]
theta2 = parameters[0][1]
theta0 = intercept[0]
 
print(parameters, intercept)
print(theta1, theta2, theta0)
 
a = -theta2 / theta1
b = -theta0 / theta1
 
x = X_train[:, 1]*a+b
plt.plot(X_train[:,1], x, 'r--')
plt.show()
 
# d)
y_test_p = LogRegression.predict(X_test)
 
confusionMatrix = confusion_matrix(y_test, y_test_p)
disp = ConfusionMatrixDisplay(confusionMatrix)
 
disp.plot()
plt.show()
 
print(classification_report(y_test, y_test_p))
 
 
# e)
trues = []
falses = []
 
for i in range(len(y_test)):
    if y_test[i] == y_test_p[i]:
        trues.append([X_test[i, 0], X_test[i, 1]])
    else:
        falses.append([X_test[i, 0], X_test[i, 1]])
 
trues = np.array(trues)
falses = np.array(falses)
 
plt.figure()
plt.scatter(x = trues[:, 0], y = trues[:, 1], marker = 'o', color = 'green')
plt.scatter(x = falses[:, 0], y = falses[:, 1], marker = 'x', color = 'red')
plt.show() 