import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap
from sklearn.base import accuracy_score
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import ConfusionMatrixDisplay, classification_report, confusion_matrix
from sklearn.model_selection import train_test_split

labels= {0:'Adelie', 1:'Chinstrap', 2:'Gentoo'}

def plot_decision_regions(X, y, classifier, resolution=0.02):
    plt.figure()
    # setup marker generator and color map
    markers = ('s', 'x', 'o', '^', 'v')
    colors = ('red', 'blue', 'lightgreen', 'gray', 'cyan')
    cmap = ListedColormap(colors[:len(np.unique(y))])
    
    # plot the decision surface
    x1_min, x1_max = X[:, 0].min() - 1, X[:, 0].max() + 1
    x2_min, x2_max = X[:, 1].min() - 1, X[:, 1].max() + 1
    xx1, xx2 = np.meshgrid(np.arange(x1_min, x1_max, resolution),
    np.arange(x2_min, x2_max, resolution))
    Z = classifier.predict(np.array([xx1.ravel(), xx2.ravel()]).T)
    Z = Z.reshape(xx1.shape)
    plt.contourf(xx1, xx2, Z, alpha=0.3, cmap=cmap)
    plt.xlim(xx1.min(), xx1.max())
    plt.ylim(xx2.min(), xx2.max())
    
    # plot class examples
    for idx, cl in enumerate(np.unique(y)):
        plt.scatter(x=X[y == cl, 0],
                    y=X[y == cl, 1],
                    alpha=0.8,
                    c=colors[idx],
                    marker=markers[idx],
                    edgecolor = 'w',
                    label=labels[cl])

# ucitaj podatke
df = pd.read_csv("penguins.csv")

# izostale vrijednosti po stupcima
print(df.isnull().sum())

# spol ima 11 izostalih vrijednosti; izbacit cemo ovaj stupac
df = df.drop(columns=['sex'])

# obrisi redove s izostalim vrijednostima
df.dropna(axis=0, inplace=True)

# kategoricka varijabla vrsta - kodiranje
df['species'].replace({'Adelie' : 0,
                        'Chinstrap' : 1,
                        'Gentoo': 2}, inplace = True)

print(df.info())

# izlazna velicina: species
output_variable = ['species']

# ulazne velicine: bill length, flipper_length
input_variables = ['bill_length_mm',
                    'flipper_length_mm']

X = df[input_variables].to_numpy()
y = df[output_variable].to_numpy()

# podjela train/test
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state = 123)

'''
Skripta zadatak_2.py ucˇitava podatkovni skup Palmer Penguins [1]. Ovaj podatkovni skup sadrži mjerenja provedena na tri razlicˇite vrste pingvina (’Adelie’, ’Chins- trap’, ’Gentoo’) na tri razlicˇita otoka u podrucˇju Palmer Station, Antarktika. Vrsta pingvina odabrana je kao izlazna velicˇina i pri tome su klase oznacˇene s cjelobrojnim vrijednostima 0, 1 i 2. Ulazne velicˇine su duljina kljuna (’bill_length_mm’) i duljina peraje u mm (’flip- per_length_mm’). Za vizualizaciju podatkovnih primjera i granice odluke u skripti je dostupna funkcija plot_decision_region.
a) Pomoc ́u stupcˇastog dijagrama prikažite koliko primjera postoji za svaku klasu (vrstu pingvina) u skupu podataka za ucˇenje i skupu podataka za testiranje. Koristite numpy funkciju unique.
b) Izgradite model logisticˇke regresije pomoc ́u scikit-learn biblioteke na temelju skupa poda- taka za ucˇenje.
c) Pronad ̄ite u atributima izgrad ̄enog modela parametre modela. Koja je razlika u odnosu na binarni klasifikacijski problem iz prvog zadatka?
d) Pozovite funkciju plot_decision_region pri cˇemu joj predajte podatke za ucˇenje i izgrad ̄eni model logisticˇke regresije. Kako komentirate dobivene rezultate?
e) Provedite klasifikaciju skupa podataka za testiranje pomoc ́u izgrad ̄enog modela logisticˇke regresije. Izracˇunajte i prikažite matricu zabune na testnim podacima. Izracˇunajte tocˇnost. Pomoc ́u classification_report funkcije izracˇunajte vrijednost cˇetiri glavne metrike
na skupu podataka za testiranje.
f) Dodajte u model još ulaznih velicˇina. Što se dogad ̄a s rezultatima klasifikacije na skupu
podataka za testiranje?
'''

# a)
classes, counts_train = np.unique(y_train, return_counts=True)
classes, counts_test = np.unique(y_test, return_counts=True)
X_axis = np.arange(len(classes))
plt.bar(X_axis - 0.2, counts_train, 0.4, label = 'Train')
plt.bar(X_axis + 0.2, counts_test, 0.4, label = 'Test') 
plt.xticks(X_axis, ['Adelie(0)', 'Chinstrap(1)', 'Gentoo(2)'])
plt.legend()
plt.show()

# b)
logReg = LogisticRegression(max_iter=120)
logReg.fit(X_train, y_train)

# c)
print(logReg.coef_)
print(logReg.intercept_)

# d)
plot_decision_regions(X_train, y_train, classifier=logReg)
plt.show()

# e)
y_prediction = logReg.predict(X_test)
disp = ConfusionMatrixDisplay(confusion_matrix(y_test,y_prediction))
disp.plot()
plt.title('Matrica zabune')
plt.show()
print(f'Točnost: {accuracy_score(y_test,y_prediction)}')
print(classification_report(y_test, y_prediction))

"""Kada govorimo o modelu logističke regresije za binarni 
klasifikacijski problem, model ima samo jedan skup parametara koji 
se odnose na težine (koeficijente) za svaku značajku (ulaznu varijablu),
te dodatni parametar za odsječak na y-osi. Ovi parametri se odnose na 
linearnu kombinaciju ulaznih značajki.
Međutim, kada se radi o višeklasnoj klasifikaciji, model logističke 
regresije treba naučiti različite skupove težina za svaku klasu kako 
bi razlikovao između više klasa. U ovom slučaju, parametri modela se 
proširuju tako da imamo po jedan skup težina za svaku klasu, kao i 
dodatne parametre odsječka za svaku klasu.
Dakle, za binarni klasifikacijski problem, model logističke regresije 
ima samo jedan skup parametara, dok za višeklasni klasifikacijski 
problem, model ima više skupova parametara, po jedan za svaku klasu."""

'''
#e)
from sklearn.metrics import confusion_matrix, accuracy_score, classification_report

y_pred = model.predict(X_test)

conf_matrix = confusion_matrix(y_test, y_pred)
print("Matrica zabune:")
print(conf_matrix)

accuracy = accuracy_score(y_test, y_pred)
print("Točnost: {:.2f}".format(accuracy))

report = classification_report(y_test, y_pred, target_names=list(labels.values()))
print("Classification Report:")
print(report)


#f)
"""Classification Report (za input_variables2):
              precision    recall  f1-score   support

      Adelie       1.00      0.93      0.96        27
   Chinstrap       0.89      1.00      0.94        17
      Gentoo       1.00      1.00      1.00        25

    accuracy                           0.97        69
   macro avg       0.96      0.98      0.97        69
weighted avg       0.97      0.97      0.97        69

Classification Report (za input_variables):
              precision    recall  f1-score   support

      Adelie       0.96      0.89      0.92        27
   Chinstrap       0.94      0.88      0.91        17
      Gentoo       0.89      1.00      0.94        25

    accuracy                           0.93        69
   macro avg       0.93      0.92      0.93        69
weighted avg       0.93      0.93      0.93        69

Klasifikacija je malo bolja s 4 ulazne veličine nego s 2, no nije značajna razlika."""

'''