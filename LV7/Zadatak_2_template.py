'''
Kvantizacija boje je proces smanjivanja broja razlicˇitih boja u digitalnoj slici, ali uzimajuc ́i u obzir da rezultantna slika vizualno bude što slicˇnija originalnoj slici. Jednostavan nacˇin kvantizacije boje može se postic ́i primjenom algoritma K srednjih vrijednosti na RGB vrijednosti elemenata originalne slike. Kvantizacija se tada postiže zamjenom vrijednosti svakog elementa originalne slike s njemu najbližim centrom. Na slici 7.3a dan je primjer originalne slike koja sadrži ukupno 106,276 boja, dok je na slici 7.3b prikazana rezultantna slika nakon kvantizacije i koja sadrži samo 5 boja koje su odred ̄ene algoritmom K srednjih vrijednosti.
1. Otvorite skriptu zadatak_2.py. Ova skripta ucˇitava originalnu RGB sliku test_1.jpg te ju transformira u podatkovni skup koji dimenzijama odgovara izrazu (7.2) pri cˇemu je n broj elemenata slike, a m je jednak 3. Koliko je razlicˇitih boja prisutno u ovoj slici?
2. Primijenite algoritam K srednjih vrijednosti koji c ́e pronac ́i grupe u RGB vrijednostima elemenata originalne slike.
3. Vrijednost svakog elementa slike originalne slike zamijeni s njemu pripadajuc ́im centrom.
4. Usporedite dobivenu sliku s originalnom. Mijenjate broj grupa K. Komentirajte dobivene
rezultate.
5. Primijenite postupak i na ostale dostupne slike.
6. Graficki prikažite ovisnost J o broju grupa K. Koristite atribut inertia objekta klase
KMeans. Možete li uociti lakat koji upuc ́uje na optimalni broj grupa?
7. Elemente slike koji pripadaju jednoj grupi prikažite kao zasebnu binarnu sliku. Što
primjecujete?
'''

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as Image
from sklearn.cluster import KMeans

# ucitaj sliku
img = Image.imread("imgs\\test_1.jpg")

# prikazi originalnu sliku
plt.figure()
plt.title("Originalna slika")
plt.imshow(img)
plt.tight_layout()
plt.show()

# pretvori vrijednosti elemenata slike u raspon 0 do 1
img = img.astype(np.float64) / 255

# transfromiraj sliku u 2D numpy polje (jedan red su RGB komponente elementa slike)
w,h,d = img.shape
img_array = np.reshape(img, (w*h, d))

# rezultatna slika
img_array_aprox = img_array.copy()

# Primjena K-means algoritma za grupiranje boja
kmeans = KMeans(n_clusters=5, init='random', n_init=5, random_state=42)  
kmeans.fit(img_array_aprox)

# Dobivanje centara grupa (boja)
cluster_centers = kmeans.cluster_centers_

labels = kmeans.labels_ #indeksi grupe svakog piksela

for i in range(len(img_array)):
    img_array_aprox[i] = cluster_centers[labels[i]]

# Oblikovanje slike natrag u originalne dimenzije
img_quantized = np.reshape(img_array_aprox, (w, h, d))

# Prikaz rezultantne slike
plt.figure()
plt.title("Kvantizirana slika s 5 boja")
plt.imshow(img_quantized)
plt.tight_layout()
plt.show()

k_values = range(1, 11)  

inertia_values = []

for k in k_values:
    kmeans = KMeans(n_clusters=k, random_state=42)
    kmeans.fit(img_array)
    inertia_values.append(kmeans.inertia_)

plt.figure(figsize=(8, 6))
plt.plot(k_values, inertia_values, marker='o', linestyle='-', color='b')
plt.xlabel('Broj grupa (K)')
plt.ylabel('Inercija')
plt.title('Ovisnost inercije o broju grupa (K)')
plt.grid(True)
plt.show()

K = 5
for i in range(K):
    binary_img = np.zeros((w*h, d))  
    binary_img[labels == i] = 1  
    binary_img = np.reshape(binary_img, (w, h, d))
    plt.figure()
    plt.imshow(binary_img, cmap='gray')  
    plt.title(f'Grupa {i+1}')
    plt.axis('off')
    plt.show()

'''Na primjeru prve slike dovoljno je čak uzeti dvije boje za dobivanje 
rezultantne slike jer se radi o tekstu kojeg je potrebno moći pročitati. 
Što se više boja uzima to je slika realnija i čitljivija, no nakon nekog 
broja boja (npr. 5) razlike su zanemarive.
'''

'''
Svaka od boja prikazana je bijelom bojom dok su sve ostale prikazane crnom. 
Vidi se gdje na rezultantnoj slici leži koja boja.
'''
