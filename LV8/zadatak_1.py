'''
Zadatak 8.4.1 MNIST podatkovni skup za izgradnju klasifikatora rukom pisanih znamenki dostupan je u okviru Keras-a. Skripta zadatak_1.py ucˇitava MNIST podatkovni skup te podatke priprema za ucˇenje potpuno povezane mreže.
1. Upoznajte se s ucˇitanim podacima. Koliko primjera sadrži skup za ucˇenje, a koliko skup za testiranje? Kako su skalirani ulazni podaci tj. slike? Kako je kodirana izlazne velicˇina? 2. Pomoc ́u matplotlib biblioteke prikažite jednu sliku iz skupa podataka za ucˇenje te ispišite
njezinu oznaku u terminal.
3. Pomoc ́u klase Sequential izgradite mrežu prikazanu na slici 8.5. Pomoc ́u metode
.summary ispišite informacije o mreži u terminal.
4. Pomoc ́u metode .compile podesite proces treniranja mreže.
5. Pokrenite ucˇenje mreže (samostalno definirajte broj epoha i velicˇinu serije). Pratite tijek ucˇenja u terminalu.
6. Izvršite evaluaciju mreže na testnom skupu podataka pomoc ́u metode .evaluate.
7. Izracˇunajte predikciju mreže za skup podataka za testiranje. Pomoc ́u scikit-learn biblioteke
prikažite matricu zabune za skup podataka za testiranje. 8. Pohranite model na tvrdi disk.
'''

import numpy as np
from tensorflow import keras
from keras import layers
from matplotlib import pyplot as plt
from sklearn.metrics import ConfusionMatrixDisplay, confusion_matrix


# Model / data parameters
num_classes = 10
input_shape = (28, 28, 1)

# train i test podaci
(x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()

# prikaz karakteristika train i test podataka
print('Train: X=%s, y=%s' % (x_train.shape, y_train.shape))
print('Test: X=%s, y=%s' % (x_test.shape, y_test.shape))

# TODO: prikazi nekoliko slika iz train skupa
for i in range(9):
    plt.subplot(330 + 1 + i)
    plt.imshow(x_train[i], cmap=plt.get_cmap('gray'))
plt.show()

# skaliranje slike na raspon [0,1]
x_train_s = x_train.astype("float32") / 255
x_test_s = x_test.astype("float32") / 255

# slike trebaju biti (28, 28, 1)
x_train_s = np.expand_dims(x_train_s, -1)
x_test_s = np.expand_dims(x_test_s, -1)

x_train_s=x_train.reshape(60000, 784)
x_test_s=x_test.reshape(10000, 784)

print("x_train shape:", x_train_s.shape)
print(x_train_s.shape[0], "train samples")
print(x_test_s.shape[0], "test samples")


# pretvori labele
y_train_s = keras.utils.to_categorical(y_train, num_classes)
y_test_s = keras.utils.to_categorical(y_test, num_classes)


# TODO: kreiraj model pomocu keras.Sequential(); prikazi njegovu strukturu

model = keras.Sequential()
model.add(layers.Input(shape=(784, )))
model.add(layers.Dense(100, activation="relu"))
model.add(layers.Dense(50, activation="relu"))
model.add(layers.Dense(10, activation="softmax"))

model.summary()

# TODO: definiraj karakteristike procesa ucenja pomocu .compile()

model.compile(loss="categorical_crossentropy",
              optimizer="adam",
              metrics=["accuracy",])

# TODO: provedi ucenje mreze
model.fit(x_train_s, y_train_s, epochs=5, batch_size=32, validation_split=0.1)
score = model.evaluate(x_test_s, y_test_s)

# TODO: Prikazi test accuracy i matricu zabune
y_predict=model.predict(x_test_s)
display=ConfusionMatrixDisplay(confusion_matrix(y_test, y_predict.argmax(axis=1)))
display.plot()
plt.show()
print("Test accuracy:", score[1])


# TODO: spremi model
model.save("models/")
