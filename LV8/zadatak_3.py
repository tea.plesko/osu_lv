'''
Zadatak 8.4.3 Napišite skriptu koja c ́e ucˇitati izgrad ̄enu mrežu iz zadatka 1. 
Nadalje, skripta treba ucˇitati sliku test.png sa diska. 
Dodajte u skriptu kod koji c ́e prilagoditi sliku za mrežu, klasificirati sliku pomoc ́u izgrad ̄ene mreže te ispisati rezultat u terminal. Promijenite sliku pomoc ́u nekog graficˇkog alata (npr. pomoc ́u Windows Paint-a nacrtajte broj 2) i ponovo pokrenite skriptu. 
Komentirajte dobivene rezultate za razlicˇite napisane znamenke.
'''

import numpy as np
from tensorflow import keras
from keras import layers, utils
from keras.models import load_model
from matplotlib import pyplot as plt
from sklearn.metrics import ConfusionMatrixDisplay, confusion_matrix

image = utils.load_img('broj.png', color_mode='grayscale', target_size=(28,28))

image = utils.img_to_array(image)
image_s = image.astype("float32") / 255
image_s = np.expand_dims(image_s, -1)
image_s = image_s.reshape(1, 784)

model = load_model('models/')

model.summary()

y_predict = model.predict(image_s)

print(y_predict.argmax())

