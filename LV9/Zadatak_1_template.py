import numpy as np
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.datasets import cifar10
from tensorflow.keras.utils import to_categorical
from matplotlib import pyplot as plt


# ucitaj CIFAR-10 podatkovni skup
(X_train, y_train), (X_test, y_test) = cifar10.load_data()

# prikazi 9 slika iz skupa za ucenje
plt.figure()
for i in range(9):
    plt.subplot(330 + 1 + i)
    plt.xticks([]),plt.yticks([])
    plt.imshow(X_train[i])

plt.show()


# pripremi podatke (skaliraj ih na raspon [0,1]])
X_train_n = X_train.astype('float32')/ 255.0
X_test_n = X_test.astype('float32')/ 255.0

# 1-od-K kodiranje
y_train = to_categorical(y_train, dtype ="uint8")
y_test = to_categorical(y_test, dtype ="uint8")

# CNN mreza
model = keras.Sequential()
model.add(layers.Input(shape=(32,32,3)))
model.add(layers.Conv2D(filters=32, kernel_size=(3, 3), activation='relu', padding='same'))
model.add(layers.MaxPooling2D(pool_size=(2, 2)))
model.add(layers.Conv2D(filters=64, kernel_size=(3, 3), activation='relu', padding='same'))
model.add(layers.MaxPooling2D(pool_size=(2, 2)))
model.add(layers.Conv2D(filters=128, kernel_size=(3, 3), activation='relu', padding='same'))
model.add(layers.MaxPooling2D(pool_size=(2, 2)))
model.add(layers.Flatten())
model.add(layers.Dense(500, activation='relu'))
model.add(layers.Dense(10, activation='softmax'))

#2
'''
model = keras.Sequential()
model.add(layers.Input(shape=(32,32,3)))
model.add(layers.Conv2D(filters=32, kernel_size=(3, 3), activation='relu', padding='same'))
model.add(layers.MaxPooling2D(pool_size=(2, 2)))
model.add(layers.Dropout(0.3))
model.add(layers.Conv2D(filters=64, kernel_size=(3, 3), activation='relu', padding='same'))
model.add(layers.MaxPooling2D(pool_size=(2, 2)))
model.add(layers.Dropout(0.3))
model.add(layers.Conv2D(filters=128, kernel_size=(3, 3), activation='relu', padding='same'))
model.add(layers.MaxPooling2D(pool_size=(2, 2)))
model.add(layers.Dropout(0.3))
model.add(layers.Flatten())
model.add(layers.Dense(500, activation='relu'))
model.add(layers.Dropout(0.3))
model.add(layers.Dense(10, activation='softmax'))

'''

model.summary()

# definiraj listu s funkcijama povratnog poziva
my_callbacks = [
    keras.callbacks.TensorBoard(log_dir = 'logs/cnn',
                                update_freq = 100)
]

model.compile(optimizer='adam',
                loss='categorical_crossentropy',
                metrics=['accuracy'])

model.fit(X_train_n,
            y_train,
            epochs = 40,
            batch_size = 64,
            callbacks = my_callbacks,
            validation_split = 0.1)


score = model.evaluate(X_test_n, y_test, verbose=0)
print(f'Tocnost na testnom skupu podataka: {100.0*score[1]:.2f}')

# Zadatak 9.4.1
""" 1) Mreža se sastoji od tri konvolucijska sloja, tri sloja sažimanja i 
dva potpuno povezana sloja od 500 i 10 neurona. Mreža ima 1,122,758 parametara.
3) Tijekom učenja mreže točnost je u jednom trenutku počela opadati na skupu
za validaciju, a vrijednost funkcije gubitka rasti. Točnost na testnom skupu podataka 
je 73.46."""

# Zadatak 9.4.2
""" Dropout sloj poboljšava vrijednosti točnosti klasifikacije i funkcije gubitka 
na skupu za validaciju jer smanjuje ovisnost modela o pojedinačnim značajkama i smanjuje
rizik od overfittinga. Točnost na testnom skupu je 74.89."""

#  Zadatak 9.4.3
""" Došlo je do ranog zaustavljanja u 11. epohi. Točnost na testnom skupu je 75.73."""

# Zadatak 9.4.4
""" 1) Jako velika veličina serije: Može ubrzati proces učenja jer će se gradijenti 
izračunati na većem uzorku podataka, ali istovremeno može dovesti do veće 
potrošnje memorije i računalnih resursa.
Jako mala veličina serije: Može dovesti do bržeg konvergiranja jer se 
češće ažuriraju težine mreže, ali može biti osjetljiva na šum u podacima 
i može zahtijevati dulje vrijeme učenja.
2) Jako mala stopa učenja: Može dovesti do sporog konvergiranja ili zapinjanja u 
lokalnim minimumima jer su koraci ažuriranja težina mali.
Jako velika stopa učenja: Može uzrokovati divergenciju ili osciliranje u 
učenju jer su koraci ažuriranja preveliki i mogu "preskočiti" minimum.
3) Izbačeni slojevi mogu dovesti do pojednostavljivanja modela i smanjenja 
potrebnih resursa za treniranje. Međutim, izbacivanje slojeva može dovesti 
do gubitka složenosti i sposobnosti modela da nauči složenije obrasce u podacima.
4) Smanjenje veličine skupa za učenje može dovesti do bržeg učenja jer će se manji 
skup podataka brže obrađivati. No, smanjenje veličine skupa za učenje također može 
dovesti do gubitka sposobnosti modela da generalizira, posebno ako se uklone važni 
dijelovi podataka ili se ne pokriju svi aspekti problema.

Zaljučak: Kombiniranje i pravilno podešavanje ovih faktora ključno je za 
uspješno treniranje neuronskih mreža."""
